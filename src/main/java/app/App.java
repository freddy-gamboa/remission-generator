package app;

import PDF.PDFGenerator;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Main application
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage primary) throws Exception {
        scene = new Scene(loadFXML("MainView"));
        scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        primary.setScene(scene);
        primary.initStyle(StageStyle.DECORATED);
        PDFGenerator.generateDocument();

        primary.show();
    }

    /**
     * Metodo para realizar el cambio de ecenas mediante el nombre de los archivos FXML pasados como parametro.
     * @param fxmlFileName Nombre del archivo FXML
     * @throws IOException
     */
    static void setRoot(String fxmlFileName) throws IOException {
        scene.setRoot(loadFXML(fxmlFileName));
    }

    private static Parent loadFXML(String fxmlFileName) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource(fxmlFileName + ".fxml"));
        return loader.load();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
